/*Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.

	Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.

	Create function which will be able to multiply two numbers.
		-Numbers must be provided as arguments.
		-Return the result of the multiplication.

	-Create a new variable called product.
		-This product should be able to receive the result of multiplication function.

	Log the value of product variable in the console.
*/

/*
	Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
*/

function addNum(num1, num2){
	console.log(num1 + num2);
}

addNum(1,2);

/*
	Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
*/

function subtractNum(num1, num2){
	console.log(num1 - num2);
}

subtractNum(100, 10);


/*
	Create function which will be able to multiply two numbers.
		-Numbers must be provided as arguments.
		-Return the result of the multiplication.
*/

function multiplyNum(num1, num2){
	return (num1 * num2);
}

/*
	-Create a new variable called product.
		-This product should be able to receive the result of multiplication function.

*/
let product = multiplyNum(5,5);

// Log the value of product variable in the console.
console.log(product);